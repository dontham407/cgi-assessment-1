package com.recipies.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@WebMvcTest(value = LogsParseController.class, secure = false)
public class LogsParseControllerTest {

    private static final Logger logger = LoggerFactory.getLogger(LogsParseControllerTest.class);

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void getLogsParseResponse() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/logParser").accept(
                MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        logger.info(String.valueOf(result.getResponse().getContentLength()));
        assertNotNull(result.getResponse());
    }
}
