package com.recipies.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@WebMvcTest(value = RecipiesController.class, secure = false)
public class RecipiesControllerTest  {

    private static final Logger logger = LoggerFactory.getLogger(RecipiesControllerTest.class);

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void retrieveAllRecipies() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/getAllRecipes").accept(
                MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        logger.info(String.valueOf(result.getResponse().getContentLength()));
        assertNotNull(result.getResponse());
    }

    @Test
    public void getFilteredRecipies() throws Exception {
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
                "/recipies?recipies=onions, mushrooms").accept(
                MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();
        assertNotNull(result.getResponse());
    }
}
