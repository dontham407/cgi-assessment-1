package com.recipies.exceptions;

/**
 * Custom exception class for the Recipe application
 */
public class RecipeException extends Exception {

    // Parameterless Constructor
    public RecipeException() {}

    /**
     * Constructor that will set messages on the exception.
     *
     * @param messages Messages
     */
    public RecipeException(String messages) {
        super(messages);
    }

}
