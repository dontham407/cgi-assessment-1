package com.recipies.resource;

import org.springframework.util.comparator.Comparators;

import java.util.Comparator;
import java.util.List;

public class Recipie implements Comparable<Recipie> {

	private String title;
	private String href;
	private String thumbnail;
	private List<String> ingredients;

	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getHref() {
		return href;
	}
	public void setHref(String href) {
		this.href = href;
	}
	public String getThumbnail() {
		return thumbnail;
	}
	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	public List<String> getIngredients() {
		return ingredients;
	}
	public void setIngredients(List<String> ingredients) {
		this.ingredients = ingredients;
	}


	@Override
	public int compareTo(Recipie o) {
		return Comparators.TITLE.compare(this,o);
	}

	public static class Comparators {
		public static Comparator<Recipie> TITLE = new Comparator<Recipie>() {
			@Override
			public int compare(Recipie o1, Recipie o2) {
				return o1.title.compareTo(o2.title);
			}
		};
	}
}