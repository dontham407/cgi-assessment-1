package com.recipies.resource;

import java.util.Comparator;

public class Description implements Comparable<Description> {

    private String message;
    private String logType;
    private Integer occurenceCount;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getLogType() {
        return logType;
    }

    public void setLogType(String logType) {
        this.logType = logType;
    }

    public Integer getOccurenceCount() {
        return occurenceCount;
    }

    public void setOccurenceCount(Integer occurenceCount) {
        this.occurenceCount = occurenceCount;
    }

    @Override
    public int compareTo(Description o) {
        return Description.Comparators.OCCURENCECOUNT.compare(this,o);
    }

    public static class Comparators {
        public static Comparator<Description> OCCURENCECOUNT = new Comparator<Description>() {
            @Override
            public int compare(Description o1, Description o2) {
                int count1 = o1.getOccurenceCount();
                int count2 = o2.getOccurenceCount();

	            /*For ascending order*/
                return count2-count1;

            }
        };
    }
}
