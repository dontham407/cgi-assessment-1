package com.recipies.controller;

import com.recipies.exceptions.RecipeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ResourceUtils;

import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

import java.util.stream.Collectors;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.recipies.resource.Recipie;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class RecipiesController {

    private static final Logger logger = LoggerFactory.getLogger(RecipiesController.class);

    /**
     *
     * @return Recipies List
     * @throws RecipeException
     * @throws IOException
     */
	@RequestMapping(method = RequestMethod.GET, path = "/getAllRecipes", produces = "application/json")
	public ResponseEntity<List<Recipie>> getRecipies() throws RecipeException, IOException {
		List<Recipie> recipieList = null;
		try {
			FileReader recipiesFile = new FileReader(ResourceUtils.getFile("classpath:receipe.json"));

			ObjectMapper objectMapper = new ObjectMapper();
			recipieList = objectMapper.readValue(recipiesFile, new TypeReference<List<Recipie>>(){});
			Collections.sort(recipieList, Recipie.Comparators.TITLE);

			throw new RecipeException("File not found information");

		} catch (RecipeException rs) {
			rs.printStackTrace();
		}
		return new ResponseEntity<List<Recipie>>(recipieList, HttpStatus.OK);
	}

    /**
     *
     * @param recipies entity collection.
     * @return
     * @throws RecipeException
     * @throws IOException
     */

	@RequestMapping(path = "/recipies", method = RequestMethod.GET, produces="application/json")
	public ResponseEntity<List<Recipie>> getRecipiesChosen(@RequestParam List<String> recipies) throws RecipeException, IOException{
        List<Recipie> recipieList = null;
        List<Recipie> recipieListNew = null;
        if(recipies.isEmpty()) {
            throw new RecipeException("List is empty");
        }
	    try {
            FileReader recipiesFile = new FileReader(ResourceUtils.getFile("classpath:receipe.json"));

            ObjectMapper objectMapper = new ObjectMapper();
            recipieList = objectMapper.readValue(recipiesFile, new TypeReference<List<Recipie>>(){});
            recipieListNew =  recipieList.stream().map(recipe -> {
                return recipe.getIngredients().containsAll(recipies)? recipe : null;
            }).filter(rec -> null!= rec).collect(Collectors.toList());
            Collections.sort(recipieListNew, Recipie.Comparators.TITLE);
            throw new RecipeException("File not found information");

        } catch (RecipeException rs) {
            rs.printStackTrace();
        }

		return new ResponseEntity<List<Recipie>>(recipieListNew, HttpStatus.OK);
	}
}