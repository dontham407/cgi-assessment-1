package com.recipies.controller;

import com.recipies.exceptions.RecipeException;
import com.recipies.resource.Description;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.*;
import java.lang.*;
import java.util.*;
import java.util.regex.*;

@RestController

public class LogsParseController {

    /**
     *
     * @param logType
     * @param limit
     * @return List of descriptions.
     * @throws RecipeException
     * @throws IOException
     */
	@RequestMapping(method = RequestMethod.GET, path = "/logParser", produces = "application/json")
	public ResponseEntity<List<Description>> parseLogFile(
			@RequestParam(value = "logType", required = true) String logType,
			@RequestParam(value = "limit", required = true) Integer limit)  throws RecipeException, IOException {

		BufferedReader br = null;
		String patternString = logType;
		List<Description> logListNew = null;

        if(logType == null) {
            throw new RecipeException("Log Type input is missed");
        } else if(limit == null) {
            throw new RecipeException("Number of Display records input does not exists");
        }

		try{
			FileInputStream fstream = new FileInputStream(ResourceUtils.getFile("classpath:logFile-2018-09-10.log"));
			br = new BufferedReader(new InputStreamReader(fstream));
			String strLine;
   			/* read log line by line */
			Pattern pattern = Pattern.compile(patternString);

			List<String> filterStringArray = new ArrayList<String>();

			while ((strLine = br.readLine()) != null) {
				Matcher matcher = pattern.matcher(strLine);
				if(matcher.find()) {
					String subStr = strLine.substring(matcher.end(),strLine.length());
					filterStringArray.add(subStr);
				}
			}

			fstream.close();
			logListNew = countFrequencies(filterStringArray, logType, limit);
            throw new RecipeException("File not found information");

        } catch (RecipeException rs) {
            rs.printStackTrace();
        }
		return new ResponseEntity<List<Description>>(logListNew, HttpStatus.OK);
	}

    /**
     *
     * @param list
     * @param logType
     * @param limit
     * @return List of descriptions.
     */
	public static List<Description> countFrequencies(List<String> list, String logType, Integer limit)
	{
		List<Description> descriptions = new ArrayList<Description>();
		//int listCountLimit = 0;

		Map<String, Integer> treeMap =
				new TreeMap<String, Integer>();
		for (String i : list) {
			Integer j = treeMap.get(i);
			treeMap.put(i, (j == null) ? 1 : j + 1);
		}

		for (Map.Entry<String, Integer> val : treeMap.entrySet()) {

			Description description = new Description();
			description.setLogType(logType);
			description.setMessage(val.getKey());
			description.setOccurenceCount(val.getValue());

			descriptions.add(description);

		}
		Collections.sort(descriptions, Description.Comparators.OCCURENCECOUNT);
		return descriptions.subList(0, limit);
	}
}